PROGRAM_PROD_FILE := out/pong++
PROGRAM_DEBUG_FILE := out/pongd++

CXXFLAGS := -Wall -Wextra -Werror -c -std=c++14
LDFLAGS := -lglfw -lGLEW -lGL

LIBGL3 := lib/linux_x64/libGL3.a
LIBDGL3 := lib/linux_x64/libdGL3.a

INCLUDE_DIR := -I include

OBJ_DIR := obj/
OBJD_DIR := objd/
OUT_DIR := out/
SRC_DIR := src/

SRC_FILES := main.cpp \
						 app.cpp \
						 ball.cpp \
						 color.cpp \
						 game_app.cpp \
						 game_renderer.cpp \
						 point.cpp \
						 racket.cpp
OBJ_FILES := $(patsubst %.cpp,$(OBJ_DIR)%.o,$(SRC_FILES))
OBJD_FILES := $(patsubst %.cpp,$(OBJD_DIR)%.o,$(SRC_FILES))

define compile_src
	@mkdir -p $(dir $(1))
  $(CXX) $(INCLUDE_DIR) $(CXXFLAGS) $(3) -o $(1) $(2)
endef

define gen_program
	@mkdir -p $(dir $(1))
	$(CXX) $(2) -o $(1) $(3) $(LDFLAGS) $(4)
	@echo 'Created program: $(1)'
endef

all: prod

$(PROGRAM_PROD_FILE): $(OBJ_FILES)
	$(call gen_program,$@,$^,$(LIBGL3))

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp
	$(call compile_src,$@,$<,-O3)

$(PROGRAM_DEBUG_FILE): $(OBJD_FILES)
	$(call gen_program,$@,$^,$(LIBDGL3),-g)

$(OBJD_DIR)%.o: $(SRC_DIR)%.cpp
	$(call compile_src,$@,$<,-Og -g -DDEBUG)

debug: $(PROGRAM_DEBUG_FILE)

prod: $(PROGRAM_PROD_FILE)

clean:
	rm -rf $(OBJ_DIR) $(OBJD_DIR) $(OUT_DIR)

build: all

rebuild: clean build
