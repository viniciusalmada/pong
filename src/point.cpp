#include "point.h"

Point::Point(int x, int y) : x(x), y(y){};

Point Point::operator+(const Point& rhs) const
{
  return { x + rhs.x, y + rhs.y };
}

bool Point::operator==(const Point& rhs) const
{
  return x == rhs.x && y == rhs.y;
}

std::ostream& operator<<(std::ostream& os, const Point& pt)
{
  os << "(" << pt.x << ", " << pt.y << ")";
  return os;
}