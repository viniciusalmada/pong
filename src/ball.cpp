#include "ball.h"

#include "color.h"
#include "point.h"

#include <cmath>

Ball::Ball(int x, int y) : cx(x), cy(y){};

RenderData Ball::getBufferData() const
{
  const int NUM_POINTS = 8;

  std::vector<RenderVertexData> vertices;
  vertices.reserve(NUM_POINTS + 1);
  vertices.push_back({ { this->cx, this->cy }, { 1.0f, 1.0f, 1.0f } });

  const double stepRad = (2 * M_PI) / NUM_POINTS;
  for (double rad = 0; rad < 2 * M_PI; rad += stepRad)
  {
    double x = std::cos(rad) * diameter / 2.0;
    double y = std::sin(rad) * diameter / 2.0;
    int px = static_cast<int>(x) + cx;
    int py = static_cast<int>(y) + cy;
    vertices.push_back({ { px, py }, { 1.0f, 1.0f, 1.0f } });
  }

  std::vector<unsigned int> indices;
  for (int i = 1; i < static_cast<int>(vertices.size()); i++)
  {
    int center = 0;
    int current = i;
    int next = i + 1 == NUM_POINTS + 1 ? 1 : i + 1;

    indices.push_back(center);
    indices.push_back(current);
    indices.push_back(next);
  }

  return { vertices, indices };
}