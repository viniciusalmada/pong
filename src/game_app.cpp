#include "game_app.h"

GameApp::GameApp()
    : ball({ 0, 0 }), racket1({ 0, Side::LEFT }), racket2({ 0, Side::RIGHT })
{
}

GameData GameApp::retrieveData() const { return { ball, racket1, racket2 }; }