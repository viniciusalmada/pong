#include "app.h"

#include <GL/glew.h>
#include <GL3/gl3_index_buffer.h>
#include <GL3/gl3_renderer.h>
#include <GL3/gl3_shader.h>
#include <GL3/gl3_vertex_array.h>
#include <GL3/gl3_vertex_buffer.h>
#include <GL3/gl3_vertex_buffer_layout.h>
#include <GLFW/glfw3.h>
#include <vector>

int main()
{
  App();
#if 0
  if (!glfwInit()) return -1;

  GLFWwindow* window = glfwCreateWindow(800, 400, "PONG++", nullptr, nullptr);

  glfwMakeContextCurrent(window);

  if (glewInit()) return -1;

  std::vector<float> vbData{
      0.0f,  0.0f, 1.0f,1.0f,1.0f,
  7.0f,  0.0f, 1.0f,1.0f,1.0f,
  5.0f,  5.0f, 1.0f,1.0f,1.0f,
  0.0f,  7.0f, 1.0f,1.0f,1.0f,
  -5.0f,  5.0f, 1.0f,1.0f,1.0f,
  -7.0f,  0.0f, 1.0f,1.0f,1.0f,
  -5.0f,  -5.0f, 1.0f,1.0f,1.0f,
  0.0f,  -7.0f, 1.0f,1.0f,1.0f,
  5.0f,  -5.0f, 1.0f,1.0f,1.0f,
  };
  /* std::vector<float> vbData{
    0.0f, -190.0f, 1.0f,   1.0f,   0.0f, 
    +0.00f, +190.0f, 1.0f,   0.0f,   1.0f,   
    +390.0f, -190.0f, 0.0f, 1.0f,   1.0f,
  }; */
  /*std::vector<float> vbData{
    -0.95f, -0.95f, 1.0f,   1.0f,   0.0f, 
    +0.00f, +0.95f, 1.0f,   0.0f,   1.0f,   
    +0.95f, -0.95f, 0.0f, 1.0f,   1.0f,
  };*/
  unsigned int dataSize =
    static_cast<unsigned int>(vbData.size() * sizeof(float));
  GL3::VertexBuffer vb{ vbData.data(), dataSize };
  GL3::VertexBufferLayout vbl =
    GL3::VertexBufferLayoutBuilder{}.pushFloat(2).pushFloat(3).getLayout();
  std::vector<unsigned int> ibData{ 0, 1, 2, 0, 2, 3, 0, 
    3, 4, 0, 4, 5, 0, 5, 6, 0, 6, 7, 0, 7, 8, 0, 8, 1 };
  GL3::IndexBuffer ib{ ibData.data(), 24 };
  GL3::VertexArray va{ vb, vbl };

  GL3::ShaderSource vertexSrc{ R"(
    #version 330 core
    const mat4 SCALE_MAT = mat4(
      1.0/400.0, 0.0, 0.0, 0.0,    // col 0
      0.0, 1.0/200.0, 0.0, 0.0,    // col 1
      0.0, 0.0, 1.0, 0.0,
      0.0, 0.0, 0.0, 1.0
    );
    layout(location = 0) in vec2 position;
    layout(location = 1) in vec3 color;
    out vec4 fragColor;
    void main()
    {
      gl_Position = SCALE_MAT * vec4(position, 1.0, 1.0);
      fragColor = vec4(color, 1.0);
    }
  )",
                               GL3::ShaderType::VERTEX };

  GL3::ShaderSource fragSrc{ R"(
    #version 330 core
    in vec4 fragColor;
    out vec4 color;
    void main()
    {
      color = fragColor;
    }
  )",
                             GL3::ShaderType::FRAGMENT };
  GL3::Shader shader{ vertexSrc, fragSrc };

  GL3::Renderer renderer{ GL3::RendererData{ va, ib, shader } };

  while (!glfwWindowShouldClose(window))
  {
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    renderer.draw();
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  glfwDestroyWindow(window);

  return 0;
#endif
}
