#include "game_renderer.h"

#include <algorithm>

GameRenderer::GameRenderer() : renderer(nullptr) {}

void GameRenderer::init(GameData data) { initRenderer(data); }

void GameRenderer::initRenderer(GameData data)
{
  auto vbLayout =
    GL3::VertexBufferLayoutBuilder{}.pushInt(2).pushFloat(3).getLayout();

  auto ballBufferData = data.ball.getBufferData();
  auto racket1BufferData = data.racket1.getBufferData();
  auto racket2BufferData = data.racket2.getBufferData();

  std::vector<RenderVertexData> verticesData;
  verticesData.insert(verticesData.end(), ballBufferData.vertices.begin(),
                      ballBufferData.vertices.end());
  verticesData.insert(verticesData.end(), racket1BufferData.vertices.begin(),
                      racket1BufferData.vertices.end());
  verticesData.insert(verticesData.end(), racket2BufferData.vertices.begin(),
                      racket2BufferData.vertices.end());
  unsigned int verticesSize = sizeof(RenderVertexData) * verticesData.size();
  auto vertexBuffer = GL3::VertexBuffer{ verticesData.data(), verticesSize };

  std::vector<unsigned int> indicesData;
  unsigned int last = 0;
  auto indicesLambda = [&indicesData, &last](unsigned int id)
  { indicesData.push_back(id + last); };

  std::for_each(ballBufferData.indices.cbegin(), ballBufferData.indices.cend(),
                indicesLambda);
  last = *(std::max_element(ballBufferData.indices.cbegin(),
                            ballBufferData.indices.cend()));
  std::for_each(racket1BufferData.indices.cbegin(),
                racket1BufferData.indices.cend(), indicesLambda);
  last = *(std::max_element(racket1BufferData.indices.cbegin(),
                            racket1BufferData.indices.cend()));
  std::for_each(racket2BufferData.indices.cbegin(),
                racket2BufferData.indices.cend(), indicesLambda);
  auto indexBuffer =
    GL3::IndexBuffer{ indicesData.data(),
                      static_cast<unsigned int>(indicesData.size()) };

  auto vertexArray = GL3::VertexArray{ vertexBuffer, vbLayout };

  auto vertexShaderSource = R"glsl(
    #version 330 core
    const mat4 SCALE_MAT = mat4(
      1.0/400.0, 0.0, 0.0, 0.0,    // col 0
      0.0, 1.0/200.0, 0.0, 0.0,    // col 1
      0.0, 0.0, 1.0, 0.0,
      0.0, 0.0, 0.0, 1.0
    );
    layout(location = 0) in ivec2 position;
    layout(location = 1) in vec3 rgbColor;
    out vec4 fragColor;
    void main()
    {
      gl_Position = SCALE_MAT * vec4(position,1.0,1.0);
      fragColor = vec4(rgbColor, 1.0);
    }
  )glsl";

  auto fragmentShaderSource = R"glsl(
    #version 330 core
    in vec4 fragColor;
    out vec4 color;
    void main()
    {
      color = fragColor;
    }
  )glsl";

  auto shader =
    GL3::Shader{ { vertexShaderSource, GL3::ShaderType::VERTEX },
                 { fragmentShaderSource, GL3::ShaderType::FRAGMENT } };

  renderer =
    new GL3::Renderer{ GL3::RendererData{ vertexArray, indexBuffer, shader } };
}

void GameRenderer::draw()
{
  if (renderer == nullptr)
  {
    std::cerr << "Renderer null" << std::endl;
    return;
  }
  renderer->draw();
}