#include "app.h"
#include <iostream>

const int App::WINDOW_WIDTH = 800;
const int App::WINDOW_HEIGHT = 400;

App::App()
{
  if (glfwInit() == GLFW_FALSE)
  {
    std::cerr << "GLFW not initialized!" << std::endl;
    return;
  }

  this->window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Pong++", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  if (glewInit() != GLEW_OK)
  {
    std::cerr << "GLEW not initialized!" << std::endl;
    glfwDestroyWindow(window);
    glfwTerminate();
    return;
  }

  std::cout << glGetString(GL_VERSION) << std::endl;
  std::cout << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

  gameRenderer.init(gameApp.retrieveData());

  while (!glfwWindowShouldClose(window)){
    glClear(GL_COLOR_BUFFER_BIT);

    glClearColor(1.0, 0.0, 1.0, 1.0);
    gameRenderer.draw();

    glfwSwapBuffers(window);

    glfwPollEvents();
  }

  glfwTerminate();
  glfwDestroyWindow(window);
}