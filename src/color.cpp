#include "color.h"

const Color Color::BLACK = Color{ 0.0f, 0.0f, 0.0f };
const Color Color::WHITE = Color{ 1.0f, 1.0f, 1.0f };

Color::Color(float r, float g, float b) : red(r), green(g), blue(b) {}

std::ostream& operator<<(std::ostream& os, const Color& c)
{
  os << "(" << c.red << ", " << c.green << ", " << c.blue << ")";
  return os;
}