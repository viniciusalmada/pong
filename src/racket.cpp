#include "racket.h"

#include "board.h"
#include "color.h"
#include "point.h"

#include <cmath>

Racket::Racket(int position, Side side) : position(position), side(side) {}

RenderData Racket::getBufferData() const
{
  std::vector<RenderVertexData> data;
  data.reserve(4);
  int halfLen = length / 2;
  int x = side == Side::LEFT ? Board::LEFT : Board::RIGHT;
  Point topLeft{ x, position + halfLen };
  Point topRight = topLeft + Point{ thickness, 0 };
  Point botRight = topLeft + Point{ thickness, -length };
  Point botLeft = topLeft + Point{ 0, -length };

  data.push_back({ topRight, Color::WHITE });
  data.push_back({ topLeft, Color::WHITE });
  data.push_back({ botLeft, Color::WHITE });
  data.push_back({ botRight, Color::WHITE });

  std::vector<unsigned int> indices{ 0, 1, 2, 0, 2, 3 };

  return RenderData{ data, indices };
}