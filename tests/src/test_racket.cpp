#include "point.h"
#include "racket.h"

#include <catch2/catch.hpp>
#include <vector>

TEST_CASE("Buffer data from Racket object")
{
  const Racket leftRacket{ 0, Racket::Side::LEFT };
  Point topRight{ -385, 25 };
  Point topLeft{ -400, 25 };
  Point botLeft{ -400, -25 };
  Point botRight{ -385, -25 };
  const std::vector<RenderVertexData> verticesExp{
    { topRight, Color::WHITE },
    { topLeft, Color::WHITE },
    { botLeft, Color::WHITE },
    { botRight, Color::WHITE },
  };
  const std::vector<unsigned int> indicesExp{ 0, 1, 2, 0, 2, 3 };

  auto data = leftRacket.getBufferData();

  REQUIRE(data.indices == indicesExp);
  REQUIRE(data.vertices[0].point == topRight);
  REQUIRE(data.vertices[1].point == topLeft);
  REQUIRE(data.vertices[2].point == botLeft);
  REQUIRE(data.vertices[3].point == botRight);
}