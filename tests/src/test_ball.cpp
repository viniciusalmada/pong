#include "ball.h"

#include <catch2/catch.hpp>
#include <cmath>
#include <vector>

TEST_CASE("Buffer data from Ball object")
{
  // Given
  const Ball ball{ 0, 0 };
  const std::vector<RenderVertexData> verticesExpec{
    { { 0, 0 }, { 1.0f, 1.0f, 1.0f } },   { { 7, 0 }, { 1.0f, 1.0f, 1.0f } },
    { { 5, 5 }, { 1.0f, 1.0f, 1.0f } },   { { 0, 7 }, { 1.0f, 1.0f, 1.0f } },
    { { -5, 5 }, { 1.0f, 1.0f, 1.0f } },  { { -7, 0 }, { 1.0f, 1.0f, 1.0f } },
    { { -5, -5 }, { 1.0f, 1.0f, 1.0f } }, { { 0, -7 }, { 1.0f, 1.0f, 1.0f } },
    { { 5, -5 }, { 1.0f, 1.0f, 1.0f } },
  };
  const std::vector<unsigned int> indicesExpec{
    0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 6, 7, 0, 7, 8, 0, 8, 1,
  };

  // When
  auto data = ball.getBufferData();

  // Then
  bool areEqual = true;
  for (int i = 0; i < static_cast<int>(data.vertices.size()); i++)
  {
    if (data.vertices[i].point.x != verticesExpec[i].point.x ||
        data.vertices[i].point.y != verticesExpec[i].point.y)
    {
      areEqual = false;
      break;
    }
  }
  bool allEqual = areEqual && data.indices == indicesExpec;
  REQUIRE(allEqual);
}