#!/usr/bin/env bash

# go to tests directory
cd "`dirname "$0"`"

# delete old executable
rm _tests

# compile tests source files
make -j`nproc`

# execute tests
./_tests
