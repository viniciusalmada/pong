#pragma once

#include <iostream>

struct Point
{
  int x;
  int y;

  Point(int x, int y);

  Point operator+(const Point& rhs) const;

  bool operator==(const Point& rhs) const;

  friend std::ostream& operator<<(std::ostream& os, const Point& pt);
};