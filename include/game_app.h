#pragma once

#include "ball.h"
#include "racket.h"
#include "game_data.h"

class GameApp
{
private:
  Ball ball;
  Racket racket1;
  Racket racket2;

public:
  GameApp();

  GameData retrieveData() const;

};