#ifndef GL3_RENDERER
#define GL3_RENDERER

#include "gl3_vertex_array.h"
#include "gl3_index_buffer.h"
#include "gl3_shader.h"

namespace GL3
{
  struct RendererData
  {
    VertexArray vertexArray;
    IndexBuffer indexBuffer;
    Shader shader;
  };

  class Renderer
  {
  private:
    RendererData rendereData;

  public:
    Renderer(RendererData data);
    ~Renderer();

    void updateVertexBuffer(const void* data, unsigned int newSize = 0);

    void updateIndexBuffer(const unsigned int* data, unsigned int newSize = 0);

    void draw();
  };
}
#endif // !GL3_RENDERER
