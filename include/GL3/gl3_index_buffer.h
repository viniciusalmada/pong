#ifndef GL3_INDEX_BUFFER
#define GL3_INDEX_BUFFER

namespace GL3
{
  class IndexBuffer
  {
  private:
    unsigned int bufferId;
    unsigned int count;

  public:
    IndexBuffer(const void* data, unsigned int count);
    void freeBuffer();

    void bind() const;
    void unbind() const;

    unsigned int getCount() const { return count; }

    void updateBuffer(const void* data, unsigned int count);
  };
}

#endif // GL3_INDEX_BUFFER