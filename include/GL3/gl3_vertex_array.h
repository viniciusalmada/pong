#ifndef GL3_VERTEX_ARRAY
#define GL3_VERTEX_ARRAY

#include "gl3_vertex_buffer.h"
#include "gl3_vertex_buffer_layout.h"

namespace GL3
{
  class VertexArray
  {
  private:
    unsigned int rendererId;
    VertexBuffer vertexBuffer;
    VertexBufferLayout layout;

  public:
    VertexArray(const VertexBuffer& vb, const VertexBufferLayout& layout);
    void freeVertexArray();

    void updateBuffer(const void* data, unsigned int newSize);

    void bind() const;
    void unbind() const;
  };
}

#endif // !GL3_VERTEX_ARRAY
