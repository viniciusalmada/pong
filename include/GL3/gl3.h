#ifndef GL3_HEADER
#define GL3_HEADER

#include "GL/glew.h"
#include <fstream>

#define GlCall(x)           \
  GL3::Log::GLClearError(); \
  x;                        \
  GL3::Log::GLLogCall(#x, __FILE__, __LINE__)

/*
static bool wasLoggerStarted = false;

static void GLStartLogger()
{

} */

namespace GL3
{
  class Log
  {
  private:
    static std::ofstream logFile;

    static bool wasLoggerStarted;

    static void GLStartLogger();

  public:
    static void PrintMessage(const char* message);

    static void GLStopLogger();

    static void GLClearError();

    static bool GLLogCall(const char* function, const char* file, int line);
  };
}

#endif // !GL3_HEADER
