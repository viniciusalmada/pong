#ifndef GL3_TEXTURE
#define GL3_TEXTURE

namespace GL3
{
  class Texture
  {
  private:
    unsigned int textureId;
    int width;
    int height;

  public:
    Texture() :textureId(0), width(0), height(0) {};
    Texture(unsigned char* buffer, int w, int h);
    void freeTexture();

    void bind(unsigned int slot) const;

    inline int getWidth() const { return width; }
    inline int getHeight() const { return height; }
  };
}

#endif // !GL3_TEXTURE
