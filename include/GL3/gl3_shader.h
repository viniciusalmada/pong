#ifndef GL3_SHADER
#define GL3_SHADER

#include "gl3.h"
#include <unordered_map>
#include <utility>

namespace GL3
{
  enum class ShaderType { VERTEX = GL_VERTEX_SHADER, FRAGMENT = GL_FRAGMENT_SHADER };

  struct ShaderSource
  {
    std::string source;
    ShaderType type;
  };

  class Shader
  {
  private:
    unsigned int programId;
    std::unordered_map<std::string, int> uniformLocations;
    std::pair<std::string, std::string> sources;

    unsigned int getUniformLocation(const std::string& name);

    unsigned int compileShader(unsigned int type, std::string source);

    void createProgram();

  public:
    static ShaderSource parseShaderString(const std::string& shaderSourcePath, ShaderType type);

  public:
    Shader(const ShaderSource& vertexShaderSource, const ShaderSource& fragmentShaderSource);
    void freeProgram();

    void bind();
    void unbind() const;

    unsigned int getId() { return programId; }

    void setUniform1i(const std::string& name, int i);
    void setUniform1iv(const std::string& name, int i, const int* data);
    void setUniform3f(const std::string& name, float f0, float f1, float f2);    
  };
}

#endif // GL3_SHADER
