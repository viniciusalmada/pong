#pragma once

#include "ball.h"
#include "racket.h"

struct GameData
{
  Ball ball;
  Racket racket1;
  Racket racket2;
};

