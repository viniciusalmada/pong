#pragma once

#include <iostream>

struct Color
{
  static const Color BLACK;
  static const Color WHITE;

  float red;
  float green;
  float blue;

  Color(float r, float g, float b);

  friend std::ostream& operator<<(std::ostream& os, const Color& pt);
};