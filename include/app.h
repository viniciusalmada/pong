#pragma once

#include "game_app.h"
#include "game_renderer.h"
#include <GLFW/glfw3.h>

class App
{
private:
  GLFWwindow *window = nullptr;
  GameApp gameApp;
  GameRenderer gameRenderer;

public:
  App();

public: // static fields
  static const int WINDOW_HEIGHT;
  static const int WINDOW_WIDTH;
};