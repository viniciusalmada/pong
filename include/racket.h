#pragma once

#include "renderable.h"

enum class Side
  {
    LEFT,
    RIGHT
  };

class Racket : public Renderable
{
private:
  const int length = 50;
  const int thickness = 15;
  int position;
  Side side;

public:
  Racket(int position, Side side);
  int getThinkness() const { return thickness; }

  virtual RenderData getBufferData() const override;
};