#pragma once

class Board
{
public:
  static const int WIDTH;
  static const int HEIGHT;
  static const int LEFT;
  static const int RIGHT;
};

const int Board::WIDTH = 800;
const int Board::HEIGHT = 400;
const int Board::LEFT = -Board::WIDTH / 2;
const int Board::RIGHT = +Board::WIDTH / 2;