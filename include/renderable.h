#pragma once

#include "render_vertex_data.h"

#include <vector>

class Renderable
{
public:
  virtual RenderData getBufferData() const = 0;
};