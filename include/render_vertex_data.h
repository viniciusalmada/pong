#pragma once

#include "point.h"
#include "color.h"

#include <cfloat>
#include <vector>

struct RenderVertexData
{
  Point point = { 0, 0 };
  Color color = Color::BLACK;
};

struct RenderData
{
  std::vector<RenderVertexData> vertices;
  std::vector<unsigned int> indices;
};