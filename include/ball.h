#pragma once

#include "renderable.h"

class Ball : public Renderable
{
private:
  int cx;
  int cy;
  const int diameter = 15;

public:
  Ball(int x, int y);

  RenderData getBufferData() const override;
};