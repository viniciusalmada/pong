#pragma once

#include "game_data.h"

#include <GL3/gl3_renderer.h>

class GameRenderer
{
private:
  GL3::Renderer* renderer;

  void initRenderer(GameData data);

public:
  GameRenderer();

  void init(GameData data);

  void draw();
};